CREATE TABLE object_value (
    object_id INT NOT NULL,
    sensor_id INT NOT NULL,
    time INT not null,
    value DECIMAL not null
);

CREATE unique INDEX object_value_uidx ON object_value(object_id, sensor_id, time);