package com.example.demo;

import java.io.Serializable;
import java.util.Objects;

public class ObjectValueId implements Serializable {
    private long objectId;
    private long sensorId;
    private long time;

    public ObjectValueId() {
    }

    public ObjectValueId(long objectId, long sensorId, long time) {
        this.objectId = objectId;
        this.sensorId = sensorId;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectValueId that = (ObjectValueId) o;
        return objectId == that.objectId &&
                sensorId == that.sensorId &&
                time == that.time;
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectId, sensorId, time);
    }
}
