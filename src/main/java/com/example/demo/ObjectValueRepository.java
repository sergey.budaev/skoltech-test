package com.example.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.NamedQuery;
import java.util.List;

public interface ObjectValueRepository extends CrudRepository<ObjectValue, Long> {
    List<ObjectValue> findByObjectId(Long objectId);

    @Query("select new com.example.demo.ObjectValueProjection(o.objectId, o.sensorId, avg(o.value)) from ObjectValue o group by o.objectId, o.sensorId order by 1, 2")
    List<ObjectValueProjection> getAvgValuesGroupedBySensorId();

    @Query("select o from ObjectValue o where object_id = ?1 and time between ?2 and ?3")
    List<ObjectValueDTO> getHistory(Long objectId, Long startTime, Long endTime);

    @Query(value = "select o.object_id as objectId, o.sensor_id as sensorId, o.time, o.value\n" +
            "from object_value o\n" +
            "inner join (\n" +
            "    select o1.object_id,\n" +
            "           o1.sensor_id,\n" +
            "           max(o1.time) max_time\n" +
            "    from object_value o1 where o1.object_id = :objectId\n" +
            "    group by 1, 2\n" +
            "    ) t\n" +
            "on o.object_id = t.object_id\n" +
            "and o.sensor_id = t.sensor_id\n" +
            "and o.time = t.max_time", nativeQuery = true)
    List<ObjectValueDTO> getLatest(@Param("objectId") Long objectId);
}
