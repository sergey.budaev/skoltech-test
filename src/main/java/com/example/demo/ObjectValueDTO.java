package com.example.demo;

import java.math.BigDecimal;

public interface ObjectValueDTO {
    Long getObjectId();

    Long getSensorId();

    Long getTime();

    BigDecimal getValue();
}
