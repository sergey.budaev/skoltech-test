package com.example.demo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ObjectValueProjection {
    private Long objectId;
    private Long sensorId;
    private BigDecimal avgValue;

    public ObjectValueProjection(Long objectId, Long sensorId, Double avgValue) {
        this.objectId = objectId;
        this.sensorId = sensorId;
        this.avgValue = BigDecimal.valueOf(avgValue);
    }
}
