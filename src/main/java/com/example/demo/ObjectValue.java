package com.example.demo;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "object_value")
@IdClass(ObjectValueId.class)
@Data
public class ObjectValue {
    @Id
    @Column(name = "object_id")
    private Long objectId;

    @Id
    @Column(name = "sensor_id")
    private Long sensorId;

    @Id
    @Column
    private Long time;

    @Column
    private BigDecimal value;

    public ObjectValue() {
    }

    public ObjectValue(Long objectId, Long sensorId, Long time, BigDecimal value) {
        this.objectId = objectId;
        this.sensorId = sensorId;
        this.time = time;
        this.value = value;
    }

}
