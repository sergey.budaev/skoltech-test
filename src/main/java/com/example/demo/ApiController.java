package com.example.demo;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ApiController {

    @Autowired
    private ObjectValueRepository repository;

    @PostMapping("/save")
    public void save(@RequestBody Iterable<ObjectValue> values) {
        repository.saveAll(values);
    }

    @GetMapping("/history")
    public List<ObjectValueDTO> getHistory(
            @RequestParam(name = "id") long objectId,
            @RequestParam(name = "start") long start,
            @RequestParam(name = "end") long end
    ) {
        return repository.getHistory(objectId, start, end);
    }

    @GetMapping("/latest")
    public List<ObjectValueDTO> getLatest(@RequestParam(name = "id", required = true) Long objectId) {
        return repository.getLatest(objectId);
    }

    @GetMapping("/avg")
    public List<ObjectValueProjection> getAvg() {
        return repository.getAvgValuesGroupedBySensorId();
    }
}
