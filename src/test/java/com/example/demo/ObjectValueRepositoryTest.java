package com.example.demo;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import static java.math.BigDecimal.valueOf;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ObjectValueRepositoryTest {
    @Autowired
    ObjectValueRepository repository;

    @BeforeEach
    public void setUp() {
        ObjectValue[] objectValues = {
                new ObjectValue(1L, 1L, 1583501397L, valueOf(10.0)),

                new ObjectValue(1L, 2L, 1583501395L, valueOf(40.0)),
                new ObjectValue(1L, 2L, 1583501397L, valueOf(10.0)),
                new ObjectValue(1L, 2L, 1583501398L, valueOf(20.0)),
                new ObjectValue(1L, 2L, 1583501399L, valueOf(30.0)),

                new ObjectValue(2L, 1L, 1583501397L, valueOf(10.0)),
                new ObjectValue(2L, 2L, 1583501398L, valueOf(20.0)),
                new ObjectValue(2L, 3L, 1583501399L, valueOf(30.0))
        };
        repository.saveAll(Arrays.asList(objectValues));
    }

    @Test
    public void getHistory() {
        List<ObjectValueDTO> actual = repository.getHistory(1L, 1583501396L, 1583501398L);
        assertEquals(3, actual.size());
        Object[] expectedValueMatrix = {
                new Object[]{1L, 1L, 1583501397L, valueOf(10.0)},
                new Object[]{1L, 2L, 1583501397L, valueOf(10.0)},
                new Object[]{1L, 2L, 1583501398L, valueOf(20.0)},
        };

        assertArrayEquals(
                expectedValueMatrix,
                objectValueListToMatrix(actual, v -> new Object[] { v.getObjectId(), v.getSensorId(), v.getTime(), v.getValue() }));
    }

    @Test
    public void getLatest() {
        List<ObjectValueDTO> actual = repository.getLatest(1L);
        assertEquals(2, actual.size());
        Object[] expectedValueMatrix = {
                new Object[]{1L, 1L, 1583501397L, valueOf(10.0)},
                new Object[]{1L, 2L, 1583501399L, valueOf(30.0)},
        };

        assertArrayEquals(
                expectedValueMatrix,
                objectValueListToMatrix(actual, v -> new Object[] { v.getObjectId(), v.getSensorId(), v.getTime(), v.getValue() }));
    }

    @Test
    public void getAvg() {
        List<ObjectValueProjection> actual = repository.getAvgValuesGroupedBySensorId();
        assertEquals(5, actual.size());
        Object[] expectedValueMatrix = {
                new Object[]{1L, 1L, valueOf(10.0)},
                new Object[]{1L, 2L, valueOf(25.0)},
                new Object[]{2L, 1L, valueOf(10.0)},
                new Object[]{2L, 2L, valueOf(20.0)},
                new Object[]{2L, 3L, valueOf(30.0)},
        };

        assertArrayEquals(
                expectedValueMatrix,
                objectValueListToMatrix(actual, v -> new Object[] { v.getObjectId(), v.getSensorId(), v.getAvgValue() }));

    }

    private <T, R> Object[] objectValueListToMatrix(List<T> list, Function<T, R> transformFn) {
        return list.stream()
                .map(transformFn)
                .toArray();
    }

}