# Sergey Budaev demo app

### Prerequesites
* PostgreSQL 11
* JDK 11

### Setting up Application
Run `psql -U username -c 'CREATE DATABASE skoltech'` to create database.\
Run `psql -U username -c 'CREATE DATABASE skoltech_test'` to create DB for tests.
Run `./gradlew compileJava test` to run tests.\

### Starting and using
Run `./gradlew bootRun` to start App. Application will be available on 8080 port. It will create table using flyway migrations.\
Run `./generate_sensor_data` without parameters (it uses parameter values by default). It will save generated data into `data.json` file.\
Then run `curl -X POST -H "Content-Type: application/json" -d @data.json http://localhost:8080/api/save` to load generated random data to database using API endpoint. It will take sometime because the file is too large (about 174MB).\
